# Allgemeines

Bisher hat dieses Projekt nur politische Akteur:innen erfasst, die in der Bundesrepublik Deutschland oder auf EU-Ebene aktiv sind.

Aktuell ist offen, wie sinnvoll eine Ausweitung auf andere Länder ist. Diese Datei dient als Zwischenspeicher für eventuelle Kandidaten-Links.

# Schweiz

## Piratenpartei

- https://chaos.social/@ppsde (Deutschsprachiger Account der Piratenpartei Schweiz)

# Österreich

## Die Partei

- https://wien.rocks/@diePARTEI (Die PARTEI Wien)
- https://graz.social/@diePARTEI_stmk (Die PARTEI Steiermark)

## Piratenpartei

- https://social.globalpirates.net/@piratenparteiat (Piratenpartei Österreich)

# Niederlande

## Volt 
- https://mastodon.nl/@voltnederland (Volt Niederlande)
- https://tukkers.online/@voltenschede (Volt Enschede)

# Dänemark

## Volt 
- https://mastodon.social/@voltdanmark (Volt Dänemark)

# Luxemburg

## Volt
- https://toot.community/@voltluxembourg (Volt Luxemburg)
