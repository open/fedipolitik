# Wie kann ich einen Pull-Request erstellen?

- Bei codeberg.org registrieren (kostenlos und grundsätzlich anonym) bzw. einloggen.
- Repo <https://codeberg.org/open/fedipolitik> öffnen
- Das Repo "forken" (d.h. eine Kopie des Repos erstellen, die aber mit dem eigenen Account verknüpft ist)
    - Dazu entweder im oberen rechten Bereich auf "Fork" klicken oder direkt diesen Link benutzen: <https://codeberg.org/open/fedipolitik/fork>
- Im eigenen Repo die Datei `README.md` bearbeiten (Stift-Symbol)
    - Die Datei ist mit mit Markdown formatiert und enthält Tabellen. Das einfachste ist, vorhandene Zeilen zu kopieren und entsprechend anzupassen. Eine vollständige Doku von Markdown gibt es z.B. hier: <https://docs.codeberg.org/markdown/>.
    - Die Funktionen **"Vorschau"** und **"Vorschau der Änderungen"** zur selbstständigen Qualitätskontrolle nutzen.
- Im Bereich "Änderungen committen" kurze Beschreibung eintragen, z.B. "XYZ Ortsverbände in SH und MV hinzugefügt" und dann den "Änderungen committen"-Button drücken
- Im originalen Repo einen Pull-Request erstellen über die Buttons "Pull-Requests" (oben, halb-links) → "Neuer Pull-Request" (rechts) oder über den Link: <https://codeberg.org/open/fedipolitik/compare/main...main>
- Im drop-down-Menü bei "pullen von" den `main`-branch aus dem eigenen (geforkten) Repo auswählen. Die entsprechenden Änderungen werden angezeigt.
- Button "Neuer-Pull-Request" klicken
- Kurze Beschreibung (kann die gleiche sein, wie beim Commit). Falls Einträge entfernt wurden, bitte nachvollziehbar begründen
- Button "Pull-Request erstellen" klicken
- Gelegentlich schauen, ob der PR schon bearbeitet wurde.
    - Manchmal gibt es noch Klärungsbedarf, was dann im Kommentarbereich des PR abgesprochen werden kann
    - Wenn es zu lange dauert: den/die Maintainer des Repos per Mastodon anpingen (siehe [README.md](README.md))