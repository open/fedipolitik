# Fedipolitik: Allgemeine Infos

Diese Datei beinhaltet eine Auflistung von Fediverse-Profilen politischer Entitäten (Parteien, Mandatsträger:innen, Amtsträger:innen, Behörden, offizielle Kanditat:innen für Wahlen, "wichtige Einzelpersonen").

Die Datei kann und soll kollektiv erweitert und aktualisiert werden. Möglichkeiten:

- Pull-Request (siehe [Anleitung](doc.md#wie-kann-ich-einen-pull-request-erstellen))
- Issue
- Nachricht an aktuelle(n) Maintainer:
  - https://social.tchncs.de/@cark
- Antwort auf: https://social.tchncs.de/@cark/105650620879066525 (Original-Toot).

Anspruch auf Vollständigkeit [uder](https://cknoll.github.io/uder) Korrektheit wird explizit negiert. Eine Erwähnung hier ist nicht gleichzusetzen mit inhaltlicher oder formaler Zustimmung oder Ablehnung. Sortiert wird innerhab der einzelnen Abschnitte grundsätzlich alphabetisch.

Diese Datei ist unter einer Creative Commons [CC-BY-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de) für die Weiterverbreitung und Nutzung freigegeben.

---

# Politische Fediverse-Profile

## Parteien und Untergliederungen

### Union

#### Christlich Demokratische Union Deutschlands (CDU)

| Wer                                      | Link                                                   |
| :--------------------------------------- | :----------------------------------------------------- |
| CDU-Bezirksverband Kappeln               | https://mastodon.social/@cdukappeln                    |
| CDU-OV Neuwied-Niederbier-Segendorf      | https://masto.ai/@CDU_Niederbieber_Segendorf           |

#### Christlich-Soziale Union (CSU)

Zum jetzigen Zeitpunkt sind keine Fediverse-Präsenzen der CSU oder ihrer Gliederungen bekannt.

#### Freie Wähler

| Instanzen        | Link                             |
| :--------------- | :------------------------------- |
| Mastodon-Instanz | https://freiewaehler.social/           |

| Wer                                              | Link                                           |
| :----------------------------------------------- | :--------------------------------------------- |
| Bundespartei                                     | https://freiewaehler.social/@Bundesvereinigung |
| Bezirksvereinigung Nordwürttemberg               | https://mastodon.online/@FW_NW                 |

### Sozialdemokratische Partei Deutschlands (SPD)

| Instanzen        | Link                             |
| :--------------- | :------------------------------- |
| Mastodon-Instanz | https://sozen.network/           |
| Mastodon-Instanz | https://sozialdemokratie.social/ |
| Mastodon-Instanz | https://spd.social/              |

| Bundesebene                 | Link                                      |
| :-------------------------- | :---------------------------------------- |
| SPD-Bundesvorstand          | https://mastodon.social/@SPDde            |

| Landesebene                 | Link                                      |
| :-------------------------- | :---------------------------------------- |
| SPD-Fraktion Bayern `(Bot)` | https://sozen.network/@spdfraktion_bayern |
| SPD Hessen                  | https://hessen.social/@spd                |
| SPD Sachsen AG Arbeit (AfA) | https://spd.social/@AfASachsen            |
| SPD-Fraktion Sachsen-Anhalt | https://mastodon.social/@spd_lsa          |

| Kreis-/Ortsebene                        | Link                                          |
| :-------------------------------------- | :-------------------------------------------- |
| SPD-KV Dudweiler                        | https://sozen.network/@SPD_Dudweiler          |
| SPD Köln-Chorweiler `(Bot)`             | https://social.cologne/@SPD_Chorweiler        |
| SPD Leipzig Ost / Nordost               | https://spd.social/@SPDLeipzigONO             |
| SPD- Ortsverein Lübeck-Schlutup          | https://spd.social/@schlutup                  |
| SPD-KV Regensburg                       | https://spd.social/@SPDKreisverbandRegensburg |
| SPD-KV Starnberg                        | https://spd.social/@kreisverband_starnberg    |

| Weitere                     | Link                                      |
| :-------------------------- | :---------------------------------------- |
| SPD-Forum Netzpolitik       | https://spd.social/@netzpolitik           |
| SPD-Freundeskreis Italien   | https://spd.social/users/spditalien       |
| SPD-Klimaforum              | https://spd.social/@klimaforum            |
| SPDqueer                    | https://mstdn.social/@SPDqueer            |
| Friedrich-Ebert-Stiftung    | https://mastodon.social/@fesonline        |

### Freie Demokratische Partei Deutschlands (FDP)

| Instanzen         | Link                                |
| :---------------- | :---------------------------------- |
| Mastodon-Instanz  | https://mastogelb.de/               |

| Kommunale Mandatsträger | Mandat                               | Link                                                 |
| :---------------------- | :----------------------------------- | :--------------------------------------------------- |
| Dominik Zoch            | Stadtbezirksbeirat in Dresden Cotta  | https://dresden.network/@dominikdresden              |

### Bündnis 90/Die Grünen (B'90/Grüne)

| Instanzen         | Link                                |
| :---------------- | :---------------------------------- |
| Mastodon-Instanz  | https://gruene.social/              |
| Pixelfed-Instanz  | https://pixel.gruene.social/        |
| Mobilizon-Instanz | https://events.gruene.social/       |
| PeerTube-Instanz  | https://peertube.netzbegruenung.de/ |

| Bundesebene
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Bündnis 90/Die Grünen im Bundestag                                | https://gruene.social/@GrueneBundestag                 |

| Landesebene                                                       | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Bündnis 90/Die Grünen Landesverband Berlin                        | https://gruene.social/@gruene_berlin                   |
| Bündnis 90/Die Grünen Fraktion Berlin                             | https://gruene.social/@gruenefraktionb                 |
| Bündnis 90/Die Grünen Landtagsfraktion Brandenburg                | https://gruene.social/@grueneltbb                      |
| Bündnis 90/Die Grünen Hessen                                      | https://gruene.social/@gruenehessen                    |
| Bündnis 90/Die Grünen Landesverband Niedersachsen                 | https://gruene.social/@gruenelvnds                     |
| Bündnis 90/Die Grünen Landesverband Nordrhein-Westfalen           | https://gruene.social/@gruenenrw                       |
| Bündnis 90/Die Grünen Landtagsfraktion Sachsen                    | https://mastodon.online/@saxgruen                      |
| Bündnis 90/Die Grünen LV Sachsen-Anhalt                           | https://gruene.social/@gruene_lsa                      |
| Bündnis 90/Die Grünen Landtagsfraktion Sachsen-Anhalt             | https://gruene.social/@GrueneFraktionLSA               |

| Kreis-/Ortsebene                                                  | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Bündnis 90/Die Grünen KV Börde (Sachsen-Anhalt)                   | https://gruene.social/@boerde                          |
| Bündnis 90/Die Grünen KV Bonn: Arbeitskreis Digitaler Wandel      | https://bonn.social/@AKDiWa                            |
| Bündnis 90/Die Grünen KV Chemnitz                                 | https://gruene.social/@gruenechemnitz                  |
| Bündnis 90/Die Grünen KV Esslingen                                | https://gruene.social/@GrueneES                        |
| Bündnis 90/Die Grünen KV Gera                                     | https://gruene.social/@GrueneGera                      |
| Bündnis 90/Die Grünen KV Gifhorn (Niedersachsen) auf Mastodon     | https://gruene.social/@gifhorn                         |
| Bündnis 90/Die Grünen KV Gifhorn (Niedersachsen) auf PeerTube     | https://peertube.netzbegruenung.de/@gruene_gifhorn     |
| Bündnis 90/Die Grünen Magdeburg                                   | https://gruene.social/@gruene_md                       |
| Bündnis 90/Die Grünen SV Oldenburg                                | https://gruene.social/@gruene_ol                       |
| Bündnis 90/Die Grünen KV Schaumburg-Stadthagen (Niedersachsen)    | https://gruene.social/@Stadthagen                      |
| Bündnis 90/Die Grünen KV Treptow-Köpenick                         | https://gruene.social/@gruene_tk                       |
| Bündnis 90/Die Grünen Urbach (Remstal, Baden-Württemberg)         | https://gruene.social/@urbach                          |
| Bündnis 90/Die Grünen OV Bad Oeynhausen                           | https://gruene.social/@gruenebo                        |
| Bündnis 90/Die Grünen OV Beelitz                                  | https://gruene.social/@GrueneBeelitz                   |
| Bündnis 90/Die Grünen OV Dietzenbach                              | https://gruene.social/@GrueneDietzenbach               |
| Bündnis 90/Die Grünen OV Lage                                     | https://gruene.social/@GrueneLage                      |
| Bündnis 90/Die Grünen OV München-Hadern                           | https://gruene.social/@gruenemuenchenhadern            |
| Bündnis 90/Die Grünen OV München-Maxvorstadt                      | https://gruene.social/@maxvorstadt                     |
| Bündnis 90/Die Grünen OV Münster-Nord                             | https://gruene.social/@muenster_nord                   |
| Bündnis 90/Die Grünen OV Rösrath                                  | https://nrw.social/@Gruene_Roesrath@gruene.social      |
| Bündnis 90/Die Grünen KV Leipzig                                  | https://gruene.social/@gruene_leipzig                  |

| Kommunale Mandatsträger | Mandat                               | Link                                                 |
| :---------------------- | :----------------------------------- | :--------------------------------------------------- |
| Alexander Bigga         | Stadtbezirksbeirat Dresden Cotta     | https://gruene.social/@albigdd                       |

### Die Linke

| Instanzen                             | Link                            |
| :------------------------------------ | :------------------------------ |
| Mastodon-Instanz (Bundestagsfraktion) | https://social.linksfraktion.de |
| Mastodon-Instanz (Partei)             | https://linke.social            |

| Wer o. Was?                                                  | Link                                           |
| :----------------------------------------------------------- | :--------------------------------------------- |
| Linksfraktion Berlin                                         | https://berlin.social/@Linksfraktion_Berlin    |
| Linksfraktion in der Bezirksverodnetenversammlung von Pankow | https://mastodon.social/@linkebvvp             |
| Linksfraktion Hessen                                         | https://hessen.social/@Linksfraktion_Hessen    |
| Die Linke KV Rheingau Taunus                                 | https://hessen.social/@Linke_RTK               |
| Die Linke KV Saale-Holzland-Kreis                            | https://mastodon.social/@dielinke_shk          |
| Rosa-Luxemburg-Stiftung                                      | https://mastodon.social/@rosaluxstiftung       |
| Die Linke OV Zittau                                          | https://mastodon.de/@linkezittau               |

#### Bundestagsfraktion

Die Linksfraktion im Bundestag betreibt eine eigene [Mastodon-Instanz](https://social.linksfraktion.de), auf der alle Abgeordneten einen Account haben. Bei vielen handelt es sich dabei (noch) um (offizielle) Bots, die auch entsprechend gekennzeichnet sind. Eine Übersicht findet man im [Profilverzeichnis der Instanz](https://social.linksfraktion.de/explore).

| Wer o. Was?                | Link                                                    |
| :------------------------- | :------------------------------------------------------ |
| Linksfraktion im Bundestag | https://social.linksfraktion.de/@linksfraktion          |
| Dietmar Bartsch            | https://social.linksfraktion.de/@dietmarbartsch         |
| Amira Mohamed Ali          | https://social.linksfraktion.de/@amira_mohamed_ali      |
| Jan Korte                  | https://social.linksfraktion.de/@jankorte               |
| Anke Domscheit-Berg        | https://social.linksfraktion.de/@ankedb                 |
| Janine Wissler             | https://social.linksfraktion.de/@janine_wissler         |
| Gökay Akbulut              | https://social.linksfraktion.de/@goekayakbulut          |
| Ali Al-Dailami             | https://social.linksfraktion.de/@ali_aldailami          |
| Matthias W. Birkwald       | https://social.linksfraktion.de/@matthias_w_birkwald    |
| Clara Bünger               | https://social.linksfraktion.de/@clarabuenger           |
| Sevim Dagdelen             | https://social.linksfraktion.de/@sevimdagdelen          |
| Klaus Ernst                | https://social.linksfraktion.de/@klausernst             |
| Susanne Ferschl            | https://social.linksfraktion.de/@susanneferschl         |
| Nicole Gohlke              | https://social.linksfraktion.de/@nicolegohlke           |
| Christian Görke            | https://social.linksfraktion.de/@christiangoerke        |
| Ates Gürpinar              | https://social.linksfraktion.de/@ates_guerpinar         |
| Gregor Gysi                | https://social.linksfraktion.de/@gregorgysi             |
| André Hahn                 | https://social.linksfraktion.de/@andrehahn              |
| Susanne Hennig-Wellsow     | https://social.linksfraktion.de/@susanne_hennig_wellsow |
| Sahra Wagenknecht          | https://social.linksfraktion.de/@sahra_wagenknecht      |
| Andrej Hunko               | https://social.linksfraktion.de/@andrejhunko            |
| Ina Latendorf              | https://social.linksfraktion.de/@ina_latendorf          |
| Caren Lay                  | https://social.linksfraktion.de/@carenlay               |
| Ralph Lenkert              | https://social.linksfraktion.de/@ralphlenkert           |
| Christian Leye             | https://social.linksfraktion.de/@christianleye          |
| Gesine Lötzsch             | https://social.linksfraktion.de/@gesine_loetzsch        |
| Thomas Lutze               | https://social.linksfraktion.de/@thomaslutze            |
| Pascal Meiser              | https://social.linksfraktion.de/@pascal_meiser          |
| Cornelia Möhring           | https://social.linksfraktion.de/@cornelia_moehring      |
| Zaklin Nastic              | https://social.linksfraktion.de/@zaklin_nastic          |
| Petra Pau                  | https://social.linksfraktion.de/@petra_pau              |
| Sören Pellmann             | https://social.linksfraktion.de/@soeren_pellmann        |
| Victor Perli               | https://social.linksfraktion.de/@victorperli            |
| Heidi Reichinnek           | https://social.linksfraktion.de/@heidi_reichinnek       |
| Martina Renner             | https://social.linksfraktion.de/@martina_renner         |
| Bernd Riexinger            | https://social.linksfraktion.de/@bernd_riexinger        |
| Petra Sitte                | https://social.linksfraktion.de/@petra_sitte            |
| Jessica Tatti              | https://social.linksfraktion.de/@jessica_tatti          |
| Alexander Ulrich           | https://social.linksfraktion.de/@alexander_ulrich       |
| Kathrin Vogler             | https://social.linksfraktion.de/@kathrinvogler          |

### Die PARTEI

| Instanzen        | Link                      |
| :--------------- | :------------------------ |
| Mastodon-Instanz | https://die-partei.social |

| Wer o. was?                        | Link                                             |
| :--------------------------------- | :----------------------------------------------- |
| Die PARTEI LV Berlin               | https://mastodon.social/@DiePARTEIBerlin         |
| Die PARTEI KV Darmstadt-Dieburg    | https://die-partei.social/@DiePARTEI_DaDi        |
| Die PARTEI KV Erding               | https://die-partei.social/@DiePARTEI_Erding      |
| Die PARTEI KV Duderstadt           | https://die-partei.social/@duderstadt            |
| Die PARTEI KV Hannover             | https://hannover.social/@diepartei               |
| Die PARTEI KV Harz                 | https://die-partei.social/@Die_PARTEI_Harz       |
| Die PARTEI KV Hof                  | https://mastodon.social/@die_PARTEI_Hof          |
| Die PARTEI OV Kronshagen           | https://norden.social/@PARTEI_Kronshagen         |
| Die PARTEI KV Landau/Süd. Weinstr. | https://die-partei.social/@parteilandau          |
| Die PARTEI KV Marburg-Biedenkopf   | https://die-partei.social/@marburgbiedenkopf     |
| Die PARTEI KV Mönchengladbach      | https://die-partei.social/@dieparteimg           |
| Die PARTEI KV Münster              | https://mastodon.social/@Die_PARTEI_MS           |
| Die PARTEI KV Nordsachsen          | https://die-partei.social/@nordsachsies          |
| Die PARTEI LV Niedersachsen        | https://die-partei.social/@niedersachsen         |
| Die PARTEI KV Nordwestmecklenburg  | https://die-partei.social/@diePARTEI_NWM         |
| Die PARTEI KV Osnabrück            | https://osna.social/@diepartei                   |
| Die PARTEI KV Pinneberg            | https://die-partei.social/@diepartei_pi          |
| Die PARTEI LV Rheinland-Pfalz      | https://die-partei.social/@DiePARTEIrlp          |
| Die PARTEI KV Reutlingen           | https://die-partei.social/@DiePARTEI_RT          |
| Die PARTEI KV Saalfeld-Rudolstadt  | https://die-partei.social/@DiePARTEI_SLF_RU      |
| Die PARTEI Steiermark              | https://graz.social/@diePARTEI_stmk              |
| Die PARTEI Wien                    | https://wien.rocks/@diePARTEI                    |

### Partei der Humanisten

| Wer o. was?                  | Link                                          |
| :--------------------------- | :-------------------------------------------- |
| Partei der Humanisten Berlin | https://eupublic.social/@DieHumanisten_Berlin |

### Piratenpartei (Piraten)

| Instanzen                | Link                           |
| :----------------------- | :----------------------------- |
| Friendica-Instanz        | https://piratenpartei.social/  |
| Friendica-Instanz (alt?) | https://pirati.ca/             |
| Mastodon-Instanz         | https://piraten-partei.social/ |
| PeerTube-Instanz         | https://www.piratentube.de/    |

| Bundesebene
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Piratenpartei Deutschland                                         | https://piratenpartei.social/profile/piratenpartei     |
| Piratenpartei Deutschland (PeerTube)                              | https://www.piratentube.de/a/ppde                      |

| Landesebene                                                       | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Piratenpartei Bayern                                              | https://piraten-partei.social/@lvbayern                |
| Piratenpartei Baden-Württemberg (BaWü / BW)                       | https://mastodon.cloud/@PiratenBW                      |
| Piratenpartei Hessen                                              | https://piratenpartei.social/profile/hessen            |
| Piratenpartei Hessen (PeerTube)                                   | https://www.piratentube.de/a/lvhessen                  |
| Piratenpartei Mecklenburg-Vorpommern                              | https://mastodon.partipirate.org/@PiratenMV            |
| Piratenpartei Niedersachsen                                       | https://piratenpartei.social/profile/piratennds        |
| Piratenpartei Nordrhein-Westfalen                                 | https://nrw.social/@piraten                            |
| Piratenpartei Rheinland-Pfalz                                     | https://mastodon.social/@piratenrlp                    |
| Piratenpartei Saarland                                            | https://mastodon.social/@piraten_saarland              |
| Piratenpartei Saarland(alt)                                       | https://mastodon.social/@piraten_saar                  |
| Piratenpartei Sachsen                                             | https://dresden.network/@piratensachsen                |
| Piratenpartei Sachsen (PeerTube)                                  | https://video.dresden.network/c/piraten_sachsen        |
| Piratenpartei Schleswig-Holstein                                  | https://piraten-partei.social/@PiratenSH               |

| Kreis-/Ortsebene                                                  | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Piratenpartei Bonn-Rhein-Sieg                                     | https://bonn.social/@piratenbonn                       |
| Piratenpartei Dresden                                             | https://pirati.ca/profile/piratendresden               |
| Piratenpartei Dresden (PeerTube)                                  | https://video.dresden.network/accounts/piraten_dresden |
| Piratenpartei Dresden-Neustadt                                    | https://dresden.network/@neustadtpiraten               |
| Piratenpartei Ennepe-Ruhr                                         | https://mastodon.social/@PiratenEN                     |
| Piratenpartei Hannover                                            | https://hannover.town/@piratenhannover                 |
| Piratenpartei Main-Taunus und Hochtaunus                          | https://mastodon.social/@taunuspiraten                 |
| Piratenpartei Leipzig                                             | https://leipzig.town/@PiratenLeipzig                   |
| Piratenpartei Leipziger Umland                                    | https://piraten-partei.social/@LeipzigerUmland         |
| Piratenpartei Leverkusen                                          | https://piratenpartei.social/profile/piraten_lev       |
| Piratenpartei Niederbayern                                        | https://piraten-partei.social/@piraten_ndb             |
| Piratenpartei Ortenaukreis                                        | https://social.tchncs.de/@PiratenOG                    |
| Piratenpartei Rhein-Erft                                          | https://pirati.ca/profile/piratenrheinerft             |

| Kommunale Mandatsträger | Mandat                               | Link                                                 |
| :---------------------- | :----------------------------------- | :--------------------------------------------------- |
| Anne Herpertz           | Stadträtin Dresden                   | https://dresden.network/@stillnotlovin               |
| Daniel Quitt            | Stadtbezirksbeirat Dresden Altstadt  | https://pirati.ca/profile/dlq                        |
| Jan Kosyk               | Stadtbezirksbeirat Dresden Neustadt  | https://dresden.network/@jankosyk                    |
| Jens Hänsch             | Stadtbezirksbeirat Dresden Lieschen  | https://dresden.network/@jhdd                        |
| Steve König             | Stadtbezirksbeirat Dresden Blasewitz | https://mastodon.social/@dangerousdetlef             |
| Stephanie Henkel        | Stadtbezirksbeirätin Dresden Cotta   | https://dresden.network/@ueckueck                    |
| " (PeerTube)            | "                                    | https://video.dresden.network/a/ueckueck             |
| Tigo Stolzenberger      | Stadtbezirksbeirat Dresden Plauen    | https://mastodon.partipirate.org/@tigo_stolzenberger |

| Arbeitsgemeinschaften (AGs)                                       | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| AG Bildung                                                        | https://piraten-partei.social/@AG_Bildung              |
| AG Drogen- und Suchtpolitik                                       | https://piratenpartei.social/profile/dropopiraten      |
| AG Europa                                                         | https://pirati.ca/profile/ag_europa                    |
| AG Gesundheit + Pflege                                            | https://piraten-partei.social/@gesundheitspiraten      |
| AG Sexarbeit                                                      | https://piraten-partei.social/@ag_sexarbeit            |

| Wer o. was?                                                           | Link                                                                  |
| :-------------------------------------------------------------------- | :-------------------------------------------------------------------- |
| Piratige Hochschulgruppe in Dresden                                   | https://chaos.social/@hopidd                                          |

### Sonstige

| Wer o. was?   | Link                                        |
| :------------ | :------------------------------------------ |
| Die Sonstigen | https://climatejustice.social/@diesonstigen |

### DiEM25/MERA25

|  Wer o. was?  | Link                                  |
| :-----------: | :------------------------------------ |
| DiEM25 Berlin | https://eupublic.social/@diem25berlin |
|    MERA25     | https://digitalcourage.social/@mera25 |

### Klimaliste

| Wer o. Was?                  | Link                                                  |
| :--------------------------- | :---------------------------------------------------- |
| Klimaliste Baden-Württemberg | https://climatejustice.social/@KlimalisteBW           |
| Klimaliste Berlin            | https://climatejustice.social/@Klimaliste_Berlin      |
| Klimaliste Bundespartei      | https://climatejustice.social/@Klimaliste_Deutschland |
| Klimafreunde Köln            | https://social.tchncs.de/@klima_freunde               |
| Klimaliste Leverkusen        | https://climatejustice.social/@klimalisteLEV          |
| Klimaliste Rems-Murr-Kreis   | https://climatejustice.social/@klimaliste_rmk         |
| Klimaliste Rhein-Neckar      | https://mas.to/@klimalisteRhNe                        |
| Klimaliste Sachsen-Anhalt    | https://climatejustice.social/@KlimalisteST           |

### ÖDP

| Landesebene                                                       | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| ÖDP Bayern                                                        | https://mas.to/@oedpbayern                             |
| ÖDP Nordrhein-Westfalen                                           | https://nrw.social/@oedp                               |
| ÖDP Thüringen                                                     | https://mastodon.world/@OedpThueringen                 |

| Kreis-/Ortsebene                                                  | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| ÖDP Stadtverband Düsseldorf und Niederrhein                       | https://nrw.social/@oedpduesseldorf                    |
| ÖDP Stadtverband München                                          | https://muenchen.social/@oedp_muenchen                 |
| ÖDP Kreisverband Landshut                                         | https://sueden.social/@oedplandshut                    |
| ÖDP Kreisverband Ruhr-Mitte                                       | https://qoto.org/@oedp_ruhrmitte                       |
| ÖDP Kreisverbände Passau-Stadt und Passau-Land                    | https://mas.to/@oedppassau                             |

### Volt

| Instanzen         | Link                           |
| :---------------- | :----------------------------- |
| Mastodon-Instanz  | https://volt.cafe/             |

| Bundesebene
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Volt Deutschland                                                  | https://mstdn.social/@voltdeutschland                  |

| Landesebene                                                       | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Volt LV Berlin                                                    | https://berlin.social/@VoltBerlin                      |
| Volt LV Bremen                                                    | https://norden.social/@voltbremen                      |
| Volt LV Sachsen-Anhalt                                            | https://det.social/@Voltsachsenanhalt                  |
| Volt LV Hessen                                                    | https://hessen.social/@VoltHessen                      |
| Volt LV Baden-Württemberg                                         | https://mastodon.social/@voltbw                        |


| Kreis-/Ortsebene                                                  | Link                                                   |
| :---------------------------------------------------------------- | :----------------------------------------------------- |
| Volt Ratsgruppe Münster                                           | https://mastodon.social/@voltmuenster                  |
| Volt KV Essen                                                     | https://nrw.social/@VoltEssen                          |
| Volt KV Eberswalde                                                | https://mastodon.social/@VoltEberswalde                |


### Regionale Vereinigungen

| Wer o. Was?                         | Link                                             |
| :---------------------------------- | :----------------------------------------------- |
| GUT Köln                            | https://social.cologne/@GUT                      |
| Zusammen Leben Rösrath              | https://nrw.social/@zlr                          |


## Politische Jugendorganisationen

### Grüne Jugend

| Wer o. Was?                              | Link                                  |
| :--------------------------------------- | :------------------------------------ |
| Grüne Jugend Netzpolitik                 | https://gruene.social/@gjnetzpolitik  |
| Grüne Jugend Brandenburg                 | https://gruene.social/@gj_bb          |
| Grüne Jugend Darmstadt/Darmstadt-Dieburg | https://gruene.social/@gjdadi         |
| Grüne Jugend Gera/Altenburg/Greiz        | https://gruene.social/@gj_gag         |
| Grüne Jugend Halle                       | https://gruene.social/@gj_halle       |
| Grüne Jugend Ludwigshafen                | https://gruene.social/@GJLu           |
| Grüne Jugend Karlsruhe                   | https://social.tchncs.de/@GJKarlsruhe |
| Jusos Dresden                            | https://dresden.network/@jusos        |

### Linksjugend

| Instanzen         | Link                           |
| :---------------- | :----------------------------- |
| Mastodon-Instanz  | https://ljs.social/            |

| Wer o. Was?                              | Link                                                |
| :--------------------------------------- | :-------------------------------------------------- |
| Linksjugend Sachsen                      | https://ljs.social/@sachsen                         |
| Linksjugend Bonn                         | https://nrw.social/@linksjugendbonn                 |
| Linksjugend Goerlitz                     | https://ljs.social/@Linksjugend_Goerlitz            |
| Linksjugend Halle                        | https://linke.social/@lijusolid_halle               |
| Linksjugend Ortenau                      | https://linke.social/@Ortenau                       |

## Behörden, Ämter und Parlamente

### EU-Behörden

| Instanzen        | Link                              |
| :--------------- | :-------------------------------- |
| Mastodon-Instanz | https://social.network.europa.eu/ |
| PeerTube-Instanz | https://tube.network.europa.eu/   |

#### Mastodon

| Wer o. Was?                                                                             | Link                                              |
| :-------------------------------------------------------------------------------------- | :------------------------------------------------ |

### Ministerien und Bundesbehörden

| Instanzen        | Link                    |
| :--------------- | :---------------------- |
| Mastodon-Instanz | https://social.bund.de/ |

| Wer o. Was?                                                                             | Link                                              |
| :-------------------------------------------------------------------------------------- | :------------------------------------------------ |
| Antidiskriminierungsstelle des Bundes                                                   | https://social.bund.de/@antidiskriminierung       |
| Auswärtiges Amt                                                                         | https://social.bund.de/@AuswaertigesAmt           |
| Bundesagentur für Arbeit (BA)                                                           | https://mastodon.social/@Bundesagentur            |
| Bundesarbeitsgericht                                                                    | https://social.bund.de/@bundesarbeitsgericht      |
| Bundesministerium für Bildung und Forschung (BMBF)                                      | https://social.bund.de/@bmbf_bund                 |
| Bundesbeauftragte/r für den Datenschutz und die Informationsfreiheit (BfDI)             | https://social.bund.de/@bfdi                      |
| Bundesfinanzhof                                                                         | https://social.bund.de/@bundesfinanzhof           |
| Bundesentwicklungsministerium (BMZ)                                                     | https://social.bund.de/@bmz                       |
| Bundesamt für die Sicherheit der nuklearen Entsorgung                                   | https://social.bund.de/@base                      |
| Bundesamt für Sicherheit in der Informationstechnik (BSI)                               | https://social.bund.de/@bsi                       |
| Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz (BMUV) | https://social.bund.de/@bmuv                  |
| Bundesverwaltungsgericht (BVerwG)                                                       | https://social.bund.de/@BVerwG_de                 |
| Bundesministerium für Wirtschaft und Klimaschutz                                        | https://social.bund.de/@bmwk                      |
| Bundesministerium für Wohnen, Stadtentwicklung und Bauwesen                             | https://social.bund.de/@BMWSB_bund                |
| Computer Emergency Response Team (CERT)                                                 | https://social.bund.de/@certbund                  |
| Föderale IT-Kooperation                                                                 | https://social.bund.de/@FITKOfoederal             |
| Forschungsnetzwerk Alterssicherung der Deutschen Rentenversicherung Bund                | https://social.bund.de/@FNA                       |
| Konferenz der unabhängigen Datenschutzaufsichtsbehörden des Bundes und der Länder (DSK) | https://social.bund.de/@dsk                       |
| Stiftung Datenschutz                                                                    | https://social.bund.de/@DS_Stiftung               |
| Zentrum für Internationale Friedenseinsätze (ZIF)                                       | https://social.bund.de/@ZIF                       |
| Bundesamt für Sicherheit in der Informationstechnik (BSI)                               | https://social.bund.de/@bsi                       |
| Informationstechnikzentrum Bund (ITZBund)                                               | https://social.bund.de/@itzbund                   |
| Bundesministerium des Inneren (BMI)                                                     | https://social.bund.de/@bmi                       |
| Bundesnetzagentur                                                                       | https://social.bund.de/@Netzausbau                |
| Ostbeauftragter der Bundesregierung                                                     | https://social.bund.de/@Ostbeauftragter           |
| Presse- und Informationsamt der Bundesregierung (Bundespresseamt; BPA)                  | https://social.bund.de/@Bundespresseamt           |
| Paul-Ehrlich-Institut                                                                   | https://social.bund.de/@PEI_Germany               |
| Statistisches Bundesamt                                                                 | https://social.bund.de/@destatis                  |
| Bundesamt für Strahlenschutz                                                            | https://social.bund.de/@strahlenschutz            |
| Bundesanstalt Technisches Hilfswerk (THW)                                               | https://social.bund.de/@THW                       |
| Umweltbundesamt                                                                         | https://social.bund.de/@Umweltbundesamt           |
| Zoll                                                                                    | https://social.bund.de/@Zoll                      |
| Sächsische Datenschutzbeauftragte                                                       | https://social.bund.de/@sdb                       |

#### inoffiziell

| Wer o. Was?                                                                                                              | Link                                  |
| :----------------------------------------------------------------------------------------------------------------------- | :------------------------------------ |
| FediNINA - Informationen des Bundesamtes für Bevölkerungsschutz und Katastrophenhilfe im Fediverse `(Inoffizielle Bots)` | https://social.prepedia.org/@FediNINA |

### Berlin

| Wer o. Was?                                                            | Link                           |
| :--------------------------------------------------------------------- | :----------------------------- |
| Berliner Beauftragte für Datenschutz und Informationsfreiheit (BlnBDI) | https://social.bund.de/@BlnBDI |

### Baden-Württemberg

| Instanzen        | Link                     |
| :--------------- | :----------------------- |
| Mastodon-Instanz | https://bawü.social/     |
| PeerTube-Instanz | https://tube.bawü.social |

| Wer o. Was?                                                                                                       | Link                                          |
| :---------------------------------------------------------------------------------------------------------------- | :-------------------------------------------- |
| Baden-Württembergischer Landtag                                                                                   | https://bawü.social/@Landtag_BW               |
| Landesregierung des Landes Baden-Württemberg (LReg BW / BaWü)                                                     | https://mastodon.social/@RegierungBW          |
| Ministerium für Umwelt, Klima und Energiewirtschaft Baden-Württemberg (UM BaWü / BW)                              | https://bawü.social/@Umweltministerium        |
| Ministerium für Verkehr Baden-Württemberg                                                                         | https://bawü.social/@VerkehrsministeriumBW    |
| Regierungspräsidium Freiburg                                                                                      | https://bawü.social/@RPFreiburg               |
| Pressestelle des Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü) | https://bawü.social/@lfdi_pressestelle        |
| Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü)                  | https://bawü.social/@lfdi                     |
| Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü)                  | https://tube.bawü.social/a/lfdi_pressestelle/ |
| Landeszentrale für politische Bildung Baden-Württemberg (LpB BW / BaWü)                                           | https://bawü.social/@lpb                      |
| Patent- und Markenzentrum Baden-Württemberg                                                                       | https://bawü.social/@pmzbw                    |

### Bayern

| Wer o. Was?                           | Link                                                  |
| :------------------------------------ | :---------------------------------------------------- |
| Bayerischer Landtag                   | https://social.bund.de/@BayerischerLandtag            |

### Hamburg

| Wer o. Was?                           | Link                                                  |
| :------------------------------------ | :---------------------------------------------------- |
| Hamburgische Bürgerschaft             | https://social.bund.de/@BuergerschaftHH               |

### Hessen

| Wer o. Was?                                                                   | Link                                           |
| :---------------------------------------------------------------------------- | :--------------------------------------------- |
| Hessischer Landtag                                                            | https://social.bund.de/@HessischerLandtag      |
| Hessische Staatskanzlei                                                       | https://social.hessen.de/@landesregierung      |
| Hessisches Ministerium der Digitale Strategie & Entwicklung                   | https://social.hessen.de/@DigitalesHessen      |
| Hessisches Ministerium der Finanzen                                           | https://social.hessen.de/@finanzen             |
| Hessisches Ministerium der Justiz                                             | https://social.hessen.de/@Justiz               |
| Hessisches Ministerium für Umwelt, Klima, Landwirtschaft & Verbraucherschutz  | https://social.hessen.de/@umwelthessen         |
| Hessisches Ministerium für Wirtschaft, Energie, Verkehr & Wohnen              | https://social.hessen.de/@wirtschafthessen     |
| Hessisches Ministerium für Wissenschaft und Kunst                             | https://social.hessen.de/@hmwk_hessen          |

### Niedersachsen

| Wer o. Was?                           | Link                                      |
| :------------------------------------ | :---------------------------------------- |
| Niedersächsischer Landtag             | https://social.bund.de/@LT_Nds            |

### Nordrhein-Westfalen

| Wer o. Was?                               | Link                           |
| :---------------------------------------- | :----------------------------- |
| Landeszentrale für politische Bildung NRW | https://bildung.social/@lpbnrw |

### Rheinland-Pfalz

| Wer o. Was?             | Link                          |
| :---------------------- | :---------------------------- |
| Landtag Rheinland-Pfalz | https://social.bund.de/@ltrlp |

### Sachsen-Anhalt

| Wer o. Was?                                                    | Link                                          |
| :------------------------------------------------------------- | :-------------------------------------------- |
| Ministeriums für Wissenschaft, Energie, Klimaschutz und Umwelt | https://mastodon.social/@mwu_sachsenanhalt    |
| Staatskanzlei                                                  | https://mastodon.social/@sachsenanhalt        |

### Thüringen

| Instanzen        | Link                                     |
| :--------------- | :--------------------------------------- |
| Mastodon-Instanz | https://freistaat-thueringen.social/     |

| Wer o. Was?                                                    | Link                                                |
| :------------------------------------------------------------- | :-------------------------------------------------- |
| Freistaat Thüringen                                            | https://freistaat-thueringen.social/@thueringen     |
| Staatskanzlei Thüringen                                        | https://freistaat-thueringen.social/@Staatskanzlei  |
| Thüringer Landtag                                              | https://social.bund.de/@thueringerlandtag           |

## Amtsträger:innen

| Wer                                                               | Link                                                      |
| :---------------------------------------------------------------- | :-------------------------------------------------------- |
| Landesdigitalminister SH Jan Philipp Albrecht                     | https://gruene.social/@janalbrecht/                       |
| Patrick Burghardt (Hessischer Digitalstaatssekretär, CDU)         | https://hessen.social/@patrickburghardt                   |
| Ulrike Grosse-Röthig (Landesvorsitzende Die Linke Thüringen)      | https://dju.social/@ulrikegrosseroethig                   |
| Priska Hinz (Hessische Ministerin für Umwelt, Klima, Landwirtschaft und Verbraucherschutz)  | https://hessen.social/@priska                   |
| Prof. Ulrich Kelber (BfDI)                                        | https://bonn.social/@ulrichkelber                         |
| Boris Rhein (Hessischer Ministerpräsident)                        | https://social.hessen.de/@BorisRhein                      |
|Lisa Paus, MdB (Bundesministerin für Familie, Senioren, Frauen und Jugend) | https://gruene.social/@LisaPaus |

## Mandatsträger:innen

| Mitglieder des Europäischen Parlaments (deutschsprachig) | Link                                           |
| :------------------------------------------------------- | :--------------------------------------------- |
| Patrick Breyer                                           | https://digitalcourage.social/@echo_pbreyer    |
| Daniel Freund                                            | https://mastodon.world/@daniel_freund          |
| Alexandra Geese                                          | https://bonn.social/@alexandrageese            |
| Sergey Lagodinsky                                        | https://gruene.social/@SLagodinsky             |
| Tiemo Wölken                                             | https://d-64.social/@woelken                   |

| Mitglieder des Bundestages          | Link                                           |
| :---------------------------------- | :--------------------------------------------- |
| Stephanie Aeffner (Grüne)           | https://gruene.social/@S_Aeffner               |
| Dagmar Andres (SPD)                 | https://social.anoxinon.de/@Dagma              |
| Andreas Audretsch (Grüne)           | https://gruene.social/@anaudretsch             |
| Maik Außendorf (Grüne)              | https://gruene.social/@AussenMa                |
| Tobias Björn Bacherle (Grüne)       | https://gruene.social/@TBBacherle              |
| Lisa Badum (Grüne)                  | https://gruene.social/@badumlisa               |
| Marco Bülow (Die PARTEI, 2002-2021) | https://ruhr.social/@marcobuelow               |
| Franziska Brantner (Grüne)          | https://gruene.social/@franziskabrantner       |
| Isabel Cademartori (SPD)            | https://rheinneckar.social/@isacademartori     |
| Jürgen Coße (SPD)                   | https://nrw.social/@MdB                        |
| Ekin Deligöz (Grüne)                | https://gruene.social/@ekindeligoez            |
| Sandra Detzer (Grüne)               | https://gruene.social/@Detzer_Sandra           |
| Karamba Diaby (SPD)                 | https://mastodon.social/@drkarambadiaby        |
| Katharina Dröge (Grüne)             | https://gruene.social/@katdro                  |
| Deborah Düring (Grüne)              | https://gruene.social/@deborahduering          |
| Harald Ebner (Grüne)                | https://gruene.social/@HaraldEbner             |
| Leon Eckert (Grüne)                 | https://gruene.social/@Leckert                 |
| Saskia Esken (SPD)                  | https://mastodon.social/@eskensaskia           |
| Sebastian Fiedler (SPD)             | https://ruhr.social/@fiedelseb                 |
| Maximilian Funke-Kaiser (FDP)       | https://troet.cafe/@mfk                        |
| Tessa Ganserer (Grüne)              | https://gruene.social/@tessaganserer           |
| Kai Gehring (Grüne)                 | https://gruene.social/@KaiGehring              |
| Jan-Niclas Gesenhues (Grüne)        | https://mstdn.social/@j_gesenhues              |
| Katrin Göring-Eckardt (Grüne)       | https://gruene.social/@GoeringEckardt          |
| Armin Grau (Grüne)                  | https://gruene.social/@ArminGrau               |
| Erhard Grundl (Grüne)               | https://gruene.social/@Erhard_Grundl           |
| Sabine Grützmacher (Grüne)          | https://gruene.social/@sabinegruetzmacher      |
| Metin Hakverdi (SPD)                | https://d-64.social/@MetinHakverdi             |
| Britta Haßelmann (Grüne)            | https://gruene.social/@BriHasselmann           |
| Kathrin Henneberger (Grüne)         | https://gruene.social/@KathrinHenneberger      |
| Thomas Hitschler (SPD)              | https://rheinneckar.social/@thomashitschler    |
| Manuel Höferlin (FDP)               | https://mastodon.social/@manuelhoeferlin       |
| Elisabeth Kaiser (SPD)              | https://mastodon.social/@ElisabethKaiser       |
| Kirsten Kappert-Gonther (Grüne)     | https://gruene.social/@kirstenKappert          |
| Michael Kellner (Grüne)             | https://gruene.social/@MichaelKellner          |
| Maria Klein-Schmeink (Grüne)        | https://nrw.social/@MKleinSchmeink             |
| Chantal Kopf (Grüne)                | https://freiburg.social/@ChantalKopf           |
| Philip Krämer (Grüne)               | https://gruene.social/@philipkraemer           |
| Misbah Khan (Grüne)                 | https://gruene.social/@MisbahKhan              |
| Jürgen Kretz (Grüne)                | https://gruene.social/@JuergenKretz            |
| Kevin Kühnert (SPD)                 | https://mastodon.social/@kuehnikev             |
| Anna Lührmann (SPD)                 | https://mastodon.social/@annaluehrmann         |
| Erik von Malottki (SPD)             | https://norden.social/@evm                     |
| Zoe Mayer (Grüne)                   | https://gruene.social/@zoe                     |
| Andreas Mehltretter (SPD)           | https://sueden.social/@mehltretter             |
| Robin Mesarosch (SPD)               | https://sueden.social/@mesarosch               |
| Irene Mihalic (Grüne)               | https://gruene.social/@IreneMihalic            |
| Beate Müller-Gemmeke (Grüne)        | https://gruene.social/@GrueneBeate             |
| Sara Nanni (Grüne)                  | https://mastodon.green/@sarananni              |
| Ophelia Nick (Grüne)                | https://gruene.social/@ophelianick             |
| Konstantin von Notz (Grüne)         | https://gruene.social/@KonstantinNotz          |
| Julian Pahlke (Grüne)               | https://mastodon.green/@j_pahlke               |
| Paula Piechotta (Grüne)             | https://gruene.social/@Paulapiechotta          |
| Filiz Polat (Grüne)                 | https://gruene.social/@FilizPolat              |
| Tabea Rößner (Grüne)                | https://gruene.social/@TabeaRoessner           |
| Johann Saathoff (SPD)               | https://mastodon.social/@saathoff              |
| Michael Sacher (Grüne)              | https://gruene.social/@Michael_Sacher          |
| Kassem Taher Saleh (Grüne)          | https://gruene.social/@kassemtahersaleh        |
| Sebastian Schäfer (Grüne)           | https://freiburg.social/@drschaefer            |
| Svenja Schulze (SPD, Bundesministerin für wirtschaftliche Zusammenarbeit & Entwicklung)  | https://muenster.im/@svenja_schulze |
| Nyke Slawik (Grüne)                 | https://social.cologne/@nykeslawik             |
| Anne Monika Spallek (Grüne)         | https://gruene.social/@aspallek                |
| Nina Stahr (Grüne)                  | https://gruene.social/@ninastahr               |
| Till Steffen (Grüne)                | https://det.social/@TillausEimsbuettel         |
| Hanna Steinmüller (Grüne)           | https://gruene.social/@HanSteinmueller         |
| Wolfgang Strengmann-Kuhn (Grüne)    | https://gruene.social/@w_sk                    |
| Margit Stumpp (Grüne)               | https://bildung.social/@margitstumpp           |
| Awet Tesfaiesus (Grüne)             | https://cultur.social/@awet                    |
| Antje Tillmann (CDU)                | https://mastodon.social/@AntjeTillmann         |
| Robin Wagener (Grüne)               | https://gruene.social/@robinwagener            |
| Johannes Wagner (Grüne)             | https://gruene.social/@yooHannes               |
| Jens Zimmermann (SPD)               | https://mastodon.social/@JensZSPD              |

Die Mitglieder der Fraktion DIE LINKE im Bundestag sind der Übersichtlichkeit halber oben unter „DIE LINKE“ aufgeführt.

| Mitglieder eines Landtages                                 | Link                                                  |
| :--------------------------------------------------------- | :---------------------------------------------------- |
| Gollaleh Ahmadi (Grüne Berlin)                             | https://gruene.social/@GollalehAhmadi                 |
| Wolfgang Aldag (Grüne Sachsen-Anhalt)                      | https://gruene.social/@WolfgangAldag                  |
| Volker Bajus (Grüne Osnabrück-Stadt)                       | https://@gruene.social/@vbajus.                       |
| Matthias Bolte (Grüne Nordrhein-Westfalen, MdL 2010-2022)  | https://gruene.social/@matthi_bolte                   |
| Miriam Dahlke (Grüne Hessen)                               | https://gruene.social/@miriamdahlke                   |
| Nina Eisenhardt (Grüne Hessen)                             | https://hessen.social/@NinaEisenhardt                 |
| Rüdiger Erben (SPD Sachsen-Anhalt)                         | https://mstdn.social/@ruedigererben                   |
| Dorothea Frederking (Grüne Sachsen-Anhalt)                 | https://gruene.social/@doro_frederking                |
| Werner Graf (Grüne Berlin)                                 | https://gruene.social/@grafwer                        |
| Bahar Haghanipour (Grüne Berlin)                           | https://mastodon.berlin/@Bahar                        |
| Anne Helm (Die Linke Berlin)                               | https://dju.social/@seeroiberjenny                    |
| Priska Hinz (Grüne Hessen)                                 | https://hessen.social/@priska                         |
| Hartmut Honka (CDU Hessen)                                 | https://hessen.social/@hartmuthonka                   |
| Bijan Kaffenberger (SPD Hessen)                            | https://hessen.social/@bijan                          |
| Daniel Karrais (FDP Baden-Württemberg)                     | https://bawü.social/@danielkarrais                    |
| Sanne Kurz (Grüne Bayern)                                  | https://gruene.social/@sannekurz_mdl                  |
| Eva Lettenbauer (Grüne Bayern)                             | https://gruene.social/@EvaLettenbauer                 |
| Torsten Leveringhaus (Grüne Hessen)                        | https://gruene.social/@torstenleveringhaus            |
| Conny Lüddemann (Grüne Sachsen-Anhalt)                     | https://gruene.social/@ConnyLueddemann                |
| Michael Lühmann (Grüne Niedersachsen)                      | https://mastodon.social/@HerrLuehmann                 |
| Olaf Meister (Grüne Sachsen-Anhalt)                        | https://gruene.social/@olaf_meister                   |
| Susi Möbbeck (SPD Sachsen-Anhalt)                          | https://machteburch.social/@Moebbeck                  |
| Jule Nagel (Die Linke Sachsen)                             | https://don.linxx.net/@luna_le                        |
| Constanze Oehlrich (Grüne Mecklenburg-Vorpommern)          | https://gruene.social/@Constanze_Oehlrich             |
| Florian Ritter (SPD Bayern)                                | https://muenchen.social/@roter_ritter                 |
| Marie Schäffer (Grüne Brandenburg)                         | https://gruene.social/@marie_schaeffer                |
| Carsten Schatz (Die Linke Berlin)                          | https://berlin.social/@schatzbln                      |
| Klara Schedlich (Grüne Berlin)                             | https://gruene.social/@klarasche                      |
| Pia Schellhammer (Grüne Rheinland-Pfalz)                   | https://gruene.social/@piaschellhammer                |
| Sebastian Schlüsselburg (Die Linke Berlin)                 | https://berlin.social/@schluesselburg                 |
| Niklas Schrader (Die Linke Berlin)                         | https://mstdn.social/@nikschrader                     |
| Katina Schubert (Die Linke Berlin)                         | https://troet.cafe/@Katina_Schubert                   |
| André Schulze (Grüne Berlin)                               | https://gruene.social/@andreschulze                   |
| Tobias Schulze (Die Linke Berlin)                          | https://mastodon.social/@TobiasSchulze                |
| Markus Stein (SPD Rheinland-Pfalz)                         | https://nahe.social/@steinmdl                         |
| Sebastian Striegel (Grüne Sachsen-Anhalt)                  | https://gruene.social/@striegse                       |
| Susan Sziborra-Seidlitz (Grüne Sachsen-Anhalt)             | https://gruene.social/@SusanSziborra                  |
| Alwin Theobald (CDU Saarland)                              | https://mastodon.social/@alwin_theobald               |
| Laura Wahl (Grüne Thüringen)                               | https://mastodon.social/@laura_wahl_                  |
| Volkhard Wille (Grüne NRW)                                 | https://gruene.social/@volkhard_wille                 |
| Lena Zagst (Grüne Hamburg)                                 | https://norden.social/@lenazagst                      |
| Claudia Maicher (Grüne Leipzig)                            | https://gruene.social/@ClaudiaMaicher                 |

| Mitglieder/Fraktionen eines Kommunalparlamentes             | Link                                   |
| :---------------------------------------------------------- | :------------------------------------- |
| Corinna Balkow (BVV Berlin-ChaWi, BÜNDNIS 90/DIE GRÜNEN)    | https://gruene.social/@coba            |
| Céline Borchers (Ortsbeirat Kiel-NDO, BÜNDNIS 90/DIE GRÜNEN)| https://mastodon.de/@koieyes           |
| Stefan Borggraefe (Rat der Stadt Witten)                    | https://mastodon.social/@BorgTenOfNine |
| Henning Brockfeld (Stadtrat Greven, BÜNDNIS 90/DIE GRÜNEN)  | https://muenster.im/@ckf               |
| Daniel Gaittet (Stadtrat Regensburg, BÜNDNIS 90/DIE GRÜNEN) | https://gruene.social/@dgaittet        |
| DISSIDENTEN-Fraktion im Dresdener Stadtrat                  | https://dresden.network/@dissidentenDD |
| Florian Fahrtmann (Stadtrat Ilsenburg (SA), SPD)            | https://det.social/@florianfahrtmann   |
| Dominic Hallau (Stadtrat Bielefeld, BÜNDNIS 90/DIE GRÜNEN)  | https://gruene.social/@DominicHallau   |
| Dr. Martin Schulte-Wissermann (Stadtrat Dresden, Piraten)   | https://dresden.network/@mswdresden    |
| Sonja Lemke (Stadtrat Dortmund, Die Linke)                  | https://ruhr.social/@sonjalemke        |
| Katja Müller (Stadtrat Halle, Die Linke)                    | https://mastodon.social/@KatjaHal      |
| Julia Probst (Stadtrat Weißenhorn, BÜNDNIS 90/DIE GRÜNEN)   | https://troet.cafe/@EinAugenschmaus    |
| Volkmar Seliger (Stadtrat Osnabrück, BÜNDNIS 90/DIE GRÜNEN) | https://gruene.social/@volkmargruen    |
| Lukas Weidinger (Stadtrat Würzburg, BÜNDNIS 90/DIE GRÜNEN)  | https://gruene.social/@lukasweidinger  |

## Weitere Personen des politischen Lebens

| Wer                       | Relevanz                                                                            | Link                                           |
| :------------------------ | :---------------------------------------------------------------------------------- | :--------------------------------------------- |
| Emily Büning              | Politische Bundesgeschäftsführerin (Grüne)                                          | https://gruene.social/@emilybuening            |
| Hans-Günter Brünker       | Bundestagswahlkandidat 2021 (Volt)                                                  | https://mastodon.social/@hgbruenker            |
| Daniel Gerber             | ehemaliges Mitglied des Sächsischen Landtags für Bündnis '90/Die Grünen, Sprecher für Digitalisierung  | http://gruene.social/@gerbsen           |
| Stephanie Henkel (ÜckÜck) | Bundestagswahlkandidatin 2021 (Piraten)                                             | https://dresden.network/@ueckueck              |
| Anna Kleimann             | Leitung Digitales SPD im Bund                                                       | https://mastodon.social/@annakleimann          |
| Benjamin Köster           | Abteilungsleiter Kommunikation im SPD Bundesvorstand                                | https://mastodon.social/@bennikoester          |
| Ruprecht Polenz           | ehemaliges Mitglied des Bundestages für die CDU                                     | https://social.dev-wiki.de/@polenz_r           |
| Felix Reda                | ehemaliges Mitglied des europäischen Parlaments für die Piraten                     | https://chaos.social/@senficon                 |
| Patrick Schiffer          | Grüne Düsseldorf, Parteivorsitzender Piratenpartei 2016-2017                        | https://gruene.social/@pakki                   |
| Dr. Marco Schrul          | Referatsleiter im Thüringer Umweltministerium (TMUEN), ehem. Leiter der Boell-Stiftung Thüringen | https://mastodon.social/@MarcoSchrul |
| Malte Spitz               | Parteirat Bündnis '90/Die Grünen                                                    | https://gruene.social/@maltespitz              |
| Matthias Stoffregen       | Sprecher Ministerium Wissenschaft, Energie, Klimaschutz & Umwelt (Sachsen-Anhalt)   | https://mastodon.social/@Matthias_Stoffregen   |
| Stefan Thurmann           | 2. stellvertretender Regierungssprecher (Sachsen-Anhalt)                            | https://machteburch.social/@StefanThurmann     |
| Norbert Walter-Borjans    | SPD-Bundesvorsitzender 2019-2021                                                    | https://social.cologne/@Nowabo                 |
| Ute Welty                 | Mitglied im Vorstand der Bundespressekonferenz                                      | https://mstdn.social/@u1w1e                    |
| Barend Wolf               | LV Die Humanisten Berlin                                                            | https://eupublic.social/@barend_wolf           |
| Gregor Voht               | Generalsekretär Freie Wähler Bund | https://freiewaehler.social/@gregor_voht    |

## Deutschsprachiges Ausland

### ÖVP

| Wer o. Was? | Link                                  |
| :---------- | :------------------------------------ |
| Georg Ecker | https://social.wien.rocks/@georgecker |

## Parteigründungsinitiativen

| Wer                             | Link                      |
| :------------------------------ | :------------------------ |
| Partei für Lebensqualität (PLQ) | https://legal.social/@plq |

## Überregionale Nichtregierungsorganisationen & Projekte

| Wer                                              | Link                                              |
| :----------------------------------------------- | :------------------------------------------------ |
| Amnesty Digital DE                               | https://social.tchncs.de/@amnesty_digital_de      |
| Anti-Atom-Netz Trier                             | https://bewegung.social/@AntiAtomNetzTrier        |
| Bedingungsloses Grundeinkommen (BGE/UBI) Fanclub | https://social.tchncs.de/@BGE_Fanclub             |
| Bewegungsstiftung                                | https://bewegung.social/@bstiftung                |
| BUNDjugend `(Bot)`                               | https://botsin.space/@BUNDjugend                  |
| BUNDjugend (Arbeitskreis Digitalisierung)        | https://climatejustice.global/@BUNDjugend_ak_digi |
| Deutscher Gewerkschaftsbund Nord                 | https://norden.social/@DGBNord/following          |
| Digitalcourage e. V.                             | https://digitalcourage.social/@digitalcourage     |
| Ende Gelände                                     | https://climatejustice.global/@ende_gelaende      |
| Finanzwende                                      | https://mastodon.online/@Finanzwende              |
| Forum Informatiker*innen für Frieden und ...     | https://mastodon.bits-und-baeume.org/@fiff_de     |
| Friedrich-Ebert-Stiftung                         | https://mastodon.social/@fesonline                |
| Fridays for Future                               | https://chaos.social/@fff                         |
| Frag den Staat                                   | https://chaos.social/@fragdenstaat                |
| Free Software Foundation Europe                  | https://mastodon.social/@fsfe                     |
| Gesellschaft für Informatik                      | https://mas.to/@informatik                        |
| Greenpeace `(Bot)`                               | https://newsbots.eu/@Greenpeace                   |
| Guerilla Tamponing                               | https://chaos.social/@guta                        |
| Gynformation                                     | https://hachyderm.io/@gynformation                |
| Junge Europäische Föderalisten Rheinland-Pfalz   | https://eupublic.social/@jefRLP                   |
| Junge Europäische Föderalisten Trier             | https://eupublic.social/@jef_trier                |
| LesbenRing e.V.                                  | https://bewegung.social/@lesbenring               |
| Liberapay                                        | https://mastodon.xyz/@Liberapay                   |
| Linux User im Bereich der Kirchen e. V. (LUKI)   | https://kirche.social/@luki                       |
| Mobilsicher.de                                   | https://mastodontech.de/@mobilsicher              |
| NOYB - Europäisches Zentrum für digitale Rechte  | https://mastodon.social/@noybeu                   |
| Otto Brenner Stiftung (Pixelfed)                 | https://pixelfed.de/OBS                           |
| Otto Brenner Stiftung (Mastodon)                 | https://mastodon.online/@OttoBrennerStiftung      |
| Parents4Future                                   | https://climatejustice.global/@parents4future     |
| Privacy is not a Crime                           | https://mastodon.online/@PrivacyIsNotACrime       |
| Prototype Fund                                   | https://mastodon.social/@PrototypeFund            |
| Queeres Zentrum Mannheim                         | https://chaos.social/@QueeresZentrumMannheim      |
| Queeres Zentrum Tübingen                         | https://social.queereszentrumtuebingen.de/@qzt    |
| Rosa-Luxemburg-Stiftung                          | https://mastodon.social/@rosaluxstiftung |
| Robin Wood                                       | https://bewegung.social/@robin_wood               |
| UNO-Flüchtlingshilfe                             | https://bonn.social/@unoflucht                    |
| XR Germany                                       | https://social.rebellion.global/@xrgermany        |

## Unsortierte Einträge

Gruppen oder individuen, die nicht in eine der obigen Kategorien passen, aber (bis zur Erstellung einer richtigen Datenbank) nicht ignoriert werden sollen

| Wer                                  | Link                               |
| :----------------------------------- | :--------------------------------- |
| Junge Europäische Föderalisten Trier | https://eupublic.social/@jef_trier |
